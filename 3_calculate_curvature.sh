#!/bin/bash
g++ -std=c++11 -pipe -Wall -O3 3_calculate_curvature.cpp -o 3_calculate_curvature -g
mkdir -p output_3_calculate_curvature
mkdir -p figs_3_calculate_curvature
for i in {100..199}
do
	NAME_IN="simmatrix_for_reduced_Boolean/simmatrix"$i".txt"
	NAME_OUT="output_3_calculate_curvature/simmatrix"$i".txt"
	echo `echo input: ${NAME_IN} output: ${NAME_OUT}`
	./3_calculate_curvature < ${NAME_IN} > ${NAME_OUT}
done

