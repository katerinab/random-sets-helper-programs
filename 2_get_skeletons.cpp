#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <numeric>
#include <vector>

// size of one side of the square bitmap
#define MAX_SIZE 400

// corresponds to B1
// o1o
// 111
// 010
#define B_SIZE 1

struct Coordinates {
  int x;
  int y;
};

/*
 * Returns true if the coordinate is inside of bitmap.
 */
bool is_valid_coordinates(Coordinates coordinates) {
  return coordinates.x >= 0 && coordinates.x < MAX_SIZE && coordinates.y >= 0 &&
         coordinates.y < MAX_SIZE;
}

/*
 * Returns true if at least one of the 4-neighbors is not in the set (=0).
 */
bool is_on_edge(Coordinates cur_position, bool bitmap[][MAX_SIZE],
                bool exp_value) {
  Coordinates up = {cur_position.x, cur_position.y - 1};
  Coordinates down = {cur_position.x, cur_position.y + 1};
  Coordinates right = {cur_position.x + 1, cur_position.y};
  Coordinates left = {cur_position.x - 1, cur_position.y};

  if (is_valid_coordinates(up) && bitmap[up.y][up.x] != exp_value)
    return true;
  if (is_valid_coordinates(down) && bitmap[down.y][down.x] != exp_value)
    return true;
  if (is_valid_coordinates(left) && bitmap[left.y][left.x] != exp_value)
    return true;
  if (is_valid_coordinates(right) && bitmap[right.y][right.x] != exp_value)
    return true;
  return false;
}

/*
 * Returns true is there is no set left (1) in the bitmap.
 */
bool is_empty(bool bitmap[][MAX_SIZE]) {
  bool no_set_found = true;
  for (int y = 0; y < MAX_SIZE; ++y) {
    for (int x = 0; x < MAX_SIZE; x++) {
      if (bitmap[y][x]) {
        return false;
      }
    }
  }
  return no_set_found;
}

/*
 * Deletes a strip on the border of the set.
 */
void delete_edge_circle(bool E2[][MAX_SIZE], Coordinates pos, int b_size) {
  float distance;
  for (int y = pos.y - b_size; y <= pos.y + b_size; y++) {
    for (int x = pos.x - b_size; x <= pos.x + b_size; x++) {
      distance = sqrt(pow(pos.x - x, 2) + pow(pos.y - y, 2));
      if (is_valid_coordinates({x, y}) && distance <= b_size) {
        E2[y][x] = false;
      }
    }
  }
}

/*
 * Adds a strip on the border of the set.
 */
void add_edge_circle(bool O[][MAX_SIZE], Coordinates pos, int b_size) {
  float distance;
  for (int y = pos.y - b_size; y <= pos.y + b_size; y++) {
    for (int x = pos.x - b_size; x <= pos.x + b_size; x++) {
      distance = sqrt(pow(pos.x - x, 2) + pow(pos.y - y, 2));
      if (is_valid_coordinates({x, y}) && distance <= b_size) {
        O[y][x] = true;
      }
    }
  }
}

/*
 * Returns coords of E1 \ O.
 */
std::vector<Coordinates> extract_skeleton_coords(bool E1[][MAX_SIZE],
                                                 bool O[][MAX_SIZE]) {
  std::vector<Coordinates> Sn;
  for (int y = 0; y < MAX_SIZE; ++y) {
    for (int x = 0; x < MAX_SIZE; x++) {
      if (E1[y][x] != O[y][x]) {
        Sn.push_back({x, y});
      }
    }
  }
  return Sn;
}

/*
 * Performs one iteration of the skeleton search algorithm.
 */
std::pair<bool, std::vector<Coordinates>> get_skeleton_i(bool E1[][MAX_SIZE],
                                                         int b_size) {
  bool set_is_empty = true;
  bool E2[MAX_SIZE][MAX_SIZE];
  bool O[MAX_SIZE][MAX_SIZE];
  std::vector<Coordinates> Sn;

  memcpy(E2, E1, sizeof(bool) * MAX_SIZE * MAX_SIZE);

  for (int y = -1; y <= MAX_SIZE; ++y) {
    for (int x = -1; x <= MAX_SIZE; x++) {
      if ((!is_valid_coordinates({x, y})) ||
          (!E1[y][x] && is_on_edge({x, y}, E1, E1[y][x]))) {
        delete_edge_circle(E2, {x, y}, b_size);
      }
    }
  }
  set_is_empty = is_empty(E2);
  if (set_is_empty) {
    Sn = extract_skeleton_coords(E1, E2);
    return make_pair(set_is_empty, Sn);
  }
  memcpy(O, E2, sizeof(bool) * MAX_SIZE * MAX_SIZE);
  for (int y = 0; y < MAX_SIZE; ++y) {
    for (int x = 0; x < MAX_SIZE; x++) {
      if (E2[y][x] && is_on_edge({x, y}, E2, E2[y][x])) {
        add_edge_circle(O, {x, y}, b_size);
      }
    }
  }
  Sn = extract_skeleton_coords(E1, O);
  memcpy(E1, E2, sizeof(bool) * MAX_SIZE * MAX_SIZE);

  return make_pair(false, Sn);
}

/*
 * Prints skeleton in a form of:
    0 = Skeleton id
    S0[0].row S0[0].column
    S0[1].row S0[1].column
    ...
    S0[|S0|].row S0[|S0|].column
    empty line
    1 = Skeleton id
    S1[0].row S1[0].column
    S1[1].row S1[1].column
    ...
 */
void print_skeleton(int n, std::vector<Coordinates> Sn) {
  printf("%d\n", n);
  for (Coordinates coo : Sn) {
    printf("%d %d\n", coo.y, coo.x);
  }
  printf("\n");
}

int main(int argc, char const *argv[]) {
  bool E1[MAX_SIZE][MAX_SIZE]; // to save the bitmap
  int value;
  int skeleton_id = 0;
  std::pair<bool, std::vector<Coordinates>> skeleton_pair;
  std::vector<Coordinates> Sn;
  bool E2_empty = false;

  // LOAD DATA
  for (int y = 0; y < MAX_SIZE; ++y) {
    for (int x = 0; x < MAX_SIZE; x++) {
      scanf("%d", &value);
      E1[y][x] = value;
    }
  }

  // GET SKELETONS
  while (!E2_empty) {
    skeleton_pair = get_skeleton_i(E1, B_SIZE);
    E2_empty = skeleton_pair.first;
    Sn = skeleton_pair.second;
    print_skeleton(skeleton_id, Sn);
    skeleton_id++;
  }
  return 0;
}
