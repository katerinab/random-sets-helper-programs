# Random sets - helper programs

Support software developed for research work of [Kateřina Helisová](http://math.feld.cvut.cz/helisova/).

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites
```
g++ compiler
c++ >= c++11
```

### Compile and run scripts

1. Compile and run script for single file using:
    + `g++ -std=c++11 -pipe -Wall -O3 1_reduce_num_components.cpp -o 1_reduce_num_components -g`
    + `./1_reduce_num_components < input.txt > output.txt`
2. Process the whole dataset using:
    + `./1_reduce_num_components.sh`

You might need to add execute privileges before running `1_reduce_num_components.sh`. To do so, run `chmod +x 1_reduce_num_components.sh` using Linux command line.

Same approach works for the other scripts as well.

### Data files

Data files are text files consisting of 400x400 bitmaps represented as zeros/ones separated by space.


## Authors

* **Kateřina Brejchová**
* **Kateřina Helisová**

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details