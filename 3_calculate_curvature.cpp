#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <numeric>
#include <vector>

// size of one side of the square bitmap
#define MAX_SIZE 400

// corresponds to B1
// o1o
// 111
// 010
#define B_SIZE 1

struct Coordinates {
  int x;
  int y;
  float curvature;
};

/*
 * Returns true if the coordinate is inside of bitmap.
 */
bool is_valid_coordinates(Coordinates coordinates) {
  return coordinates.x >= 0 && coordinates.x < MAX_SIZE && coordinates.y >= 0 &&
         coordinates.y < MAX_SIZE;
}

/*
 * Returns true if at least one of the 4-neighbors is not in the set (=0).
 */
bool is_on_edge(Coordinates cur_position, bool bitmap[][MAX_SIZE]) {
  Coordinates up = {cur_position.x, cur_position.y - 1};
  Coordinates down = {cur_position.x, cur_position.y + 1};
  Coordinates right = {cur_position.x + 1, cur_position.y};
  Coordinates left = {cur_position.x - 1, cur_position.y};

  if (!is_valid_coordinates(up) || !bitmap[up.y][up.x])
    return true;
  if (!is_valid_coordinates(down) || !bitmap[down.y][down.x])
    return true;
  if (!is_valid_coordinates(left) || !bitmap[left.y][left.x])
    return true;
  if (!is_valid_coordinates(right) || !bitmap[right.y][right.x])
    return true;
  return false;
}

/*
 * Returns curvature in the b_size-surroundings of given pixel.
 */
float get_curvature(bool bitmap[][MAX_SIZE], Coordinates pos, int b_size) {
  float distance;
  float curvature = 0.0;
  float num_to_average = 0.0;

  for (int y = pos.y - b_size; y <= pos.y + b_size; y++) {
    for (int x = pos.x - b_size; x <= pos.x + b_size; x++) {
      distance = sqrt(pow(pos.x - x, 2) + pow(pos.y - y, 2));
      if (is_valid_coordinates({x, y}) && distance <= b_size && distance > 0) {
        curvature += bitmap[y][x];
        num_to_average += 1;
      }
    }
  }
  return curvature / num_to_average;
}

/*
 * Prints curvature of border points in a form of:
    border_point[0].row border_point[0].column
    border_point[1].row border_point[1].column
    ...
    border_point[|border_points|].row border_point[|border_points|].column
    empty line
 */
void print_curvature(std::vector<Coordinates> curvature_coords) {
  for (Coordinates coo : curvature_coords) {
    printf("%d %d %f\n", coo.y, coo.x, coo.curvature);
  }
  printf("\n");
}

int main(int argc, char const *argv[]) {
  bool bitmap[MAX_SIZE][MAX_SIZE]; // to save the bitmap
  int value;
  float curvature;
  std::vector<Coordinates> curvature_coords;

  // LOAD DATA
  for (int y = 0; y < MAX_SIZE; ++y) {
    for (int x = 0; x < MAX_SIZE; x++) {
      scanf("%d", &value);
      bitmap[y][x] = value;
    }
  }

  // GET CURVATURE
  for (int y = 0; y < MAX_SIZE; ++y) {
    for (int x = 0; x < MAX_SIZE; x++) {
      if (bitmap[y][x] && is_on_edge({x, y}, bitmap)) {
        curvature = get_curvature(bitmap, {x, y}, B_SIZE);
        curvature_coords.push_back({x, y, curvature});
      }
    }
  }

  print_curvature(curvature_coords);
  return 0;
}
