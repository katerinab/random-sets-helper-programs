import matplotlib.pyplot as plt
import numpy as np





if __name__ == '__main__':
    for i in range(100, 200):
        file_orig = f'simmatrix_for_reduced_Boolean/simmatrix{i}.txt'
        file_skeleton = f'output_2_get_skeletons/simmatrix{i}.txt'

        skeleton_im = np.zeros((400,400))
        with open(file_skeleton) as f:
            f_cont = [line[2:].split("\n") for line in f.read().split("\n\n")[:-1]]
            for line in f_cont:
                for item in line:
                    if item == '':
                        break
                    y, x = (int(i) for i in item.split(' '))
                    skeleton_im[y, x] = 1

        with open(file_orig) as orig:
            lines = orig.readlines()
            orig_set_im = np.array([[int(i) for i in line[:-2].split(" ")] for line in lines])

        fig, (ax1, ax2) = plt.subplots(1,2)
        ax1.set_title('Set')
        ax1.imshow(orig_set_im)

        ax2.set_title('Skeleton')
        ax2.imshow(skeleton_im)

        plt.tight_layout()
        # plt.show()
        plt.savefig(f'figs_2_get_skeletons/simmatrix{i}.png')
        plt.close()