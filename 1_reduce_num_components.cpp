#include <iostream>
#include <stack>
#include <vector>
// size of one side of the square bitmap
#define MAX_SIZE 400
// value in range (0, 1), i.e. set 0.6 to delete component with probability 60%
#define DELETE_PROBABILITY 0.5
// do not change
#define UNASSIGNED_COMPONENT 0

struct Coordinates {
  int x;
  int y;
};

/*
 * Returns true if the coordinate is inside of bitmap.
 */
bool is_valid_coordinates(Coordinates coordinates) {
  return coordinates.x >= 0 && coordinates.x < MAX_SIZE && coordinates.y >= 0 &&
         coordinates.y < MAX_SIZE;
}

/*
 * Returns whether the curent component should be deleted based on given
 * probability.
 */
bool should_delete_random() {
  return ((double)rand() / RAND_MAX) < DELETE_PROBABILITY;
}

/*
 * Returns all valid neighbors from 4-neighborhood of given coordinate.
 */
std::vector<Coordinates> get_neighbors(Coordinates cur_position) {
  std::vector<Coordinates> neighbors;
  Coordinates up = {cur_position.x, cur_position.y - 1};
  Coordinates down = {cur_position.x, cur_position.y + 1};
  Coordinates right = {cur_position.x + 1, cur_position.y};
  Coordinates left = {cur_position.x - 1, cur_position.y};

  if (is_valid_coordinates(up))
    neighbors.push_back(up);
  if (is_valid_coordinates(down))
    neighbors.push_back(down);
  if (is_valid_coordinates(left))
    neighbors.push_back(left);
  if (is_valid_coordinates(right))
    neighbors.push_back(right);
  return neighbors;
}

/*
 * Runs DFS to mark all bits belonging to the same component as cur_position.
 */
void dfs_components(Coordinates cur_position, int component_id,
                    bool pixels[][MAX_SIZE], int components[][MAX_SIZE],
                    bool should_delete) {
  std::stack<Coordinates> stack;
  Coordinates q_position;
  stack.push(cur_position);

  if (should_delete) {
    pixels[cur_position.y][cur_position.x] = 0;
  }
  components[cur_position.y][cur_position.x] = component_id;

  while (!stack.empty()) {
    q_position = stack.top();
    stack.pop();
    for (Coordinates neighbor : get_neighbors(q_position)) {
      if (pixels[neighbor.y][neighbor.x] &&
          components[neighbor.y][neighbor.x] == UNASSIGNED_COMPONENT) {
        stack.push(neighbor);
        if (should_delete) {
          pixels[neighbor.y][neighbor.x] = 0;
        }
        components[neighbor.y][neighbor.x] = component_id;
      }
    }
  }
}

int main(int argc, char const *argv[]) {
  bool pixels[MAX_SIZE][MAX_SIZE];         // to save the bitmap
  int components[MAX_SIZE][MAX_SIZE] = {}; // initializes all elements to 0
  int component_id = 1;
  int value;

  // LOAD DATA
  for (int y = 0; y < MAX_SIZE; ++y) {
    for (int x = 0; x < MAX_SIZE; x++) {
      scanf("%d", &value);
      pixels[y][x] = value;
    }
  }

  // FIND AND DELETE COMPONENTS
  for (int y = 0; y < MAX_SIZE; ++y) {
    for (int x = 0; x < MAX_SIZE; x++) {
      if (pixels[y][x] && components[y][x] == UNASSIGNED_COMPONENT) {
        dfs_components({x, y}, component_id, pixels, components,
                       should_delete_random());
        component_id++;
      }
    }
  }

  // PRINT DATA
  for (int y = 0; y < MAX_SIZE; ++y) {
    for (int x = 0; x < MAX_SIZE; x++) {
      printf("%d ", pixels[y][x]);
    }
    printf("\n");
  }
  return 0;
}
