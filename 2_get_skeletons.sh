#!/bin/bash
g++ -std=c++11 -pipe -Wall -O3 2_get_skeletons.cpp -o 2_get_skeletons -g
mkdir -p output_2_get_skeletons
mkdir -p figs_2_get_skeletons
for i in {100..199}
do
	NAME_IN="simmatrix_for_reduced_Boolean/simmatrix"$i".txt"
	NAME_OUT="output_2_get_skeletons/simmatrix"$i".txt"
	echo `echo input: ${NAME_IN} output: ${NAME_OUT}`
	./2_get_skeletons < ${NAME_IN} > ${NAME_OUT}
done

