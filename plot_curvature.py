import matplotlib.pyplot as plt
import numpy as np





if __name__ == '__main__':
    for i in range(100, 200):
        file_orig = f'simmatrix_for_reduced_Boolean/simmatrix{i}.txt'
        file_curvature = f'output_3_calculate_curvature/simmatrix{i}.txt'

        curvature_im = np.zeros((400,400))
        with open(file_curvature) as f:
            lines = f.readlines()[:-1]
            for line in lines:
                y, x, curv = (float(i) for i in line[:-1].split(' '))
                y,x = int(y), int(x)
                curvature_im[y, x] = curv

        with open(file_orig) as orig:
            lines = orig.readlines()
            orig_set_im = np.array([[int(i) for i in line[:-2].split(" ")] for line in lines])

        fig, (ax1, ax2) = plt.subplots(1,2)
        ax1.set_title('Set')
        ax1.imshow(orig_set_im)

        ax2.set_title('Curvature')
        ax2.imshow(curvature_im)

        plt.tight_layout()
        # plt.show()
        plt.savefig(f'figs_3_calculate_curvature/simmatrix{i}.png')
        plt.close()