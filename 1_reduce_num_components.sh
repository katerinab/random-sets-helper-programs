#!/bin/bash
g++ -std=c++11 -pipe -Wall -O3 1_reduce_num_components.cpp -o 1_reduce_num_components -g
mkdir -p output_1_reduce_num_components
for i in {100..199}
do
	NAME_IN="simmatrix_for_reduced_Boolean/simmatrix"$i".txt"
	NAME_OUT="output_1_reduce_num_components/simmatrix"$i".txt"
	echo `echo input: ${NAME_IN} output: ${NAME_OUT}`
	./1_reduce_num_components < ${NAME_IN} > ${NAME_OUT}
done

